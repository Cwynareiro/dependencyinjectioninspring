package withoutInjection;

public class Bottle {

	private Drink drink; 
	
	public Bottle() {
		drink = new CocaCola();
	}
	public void drinkBottle() {
		drink.drink();
	}
}
