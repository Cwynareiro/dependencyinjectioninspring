package spring.fieldInjection;

import org.springframework.stereotype.Component;

@Component
class CocaCola implements Drink {
	
	@Override
	public void drink() {
		System.out.println("I'm drinking Cola");
	}
}
