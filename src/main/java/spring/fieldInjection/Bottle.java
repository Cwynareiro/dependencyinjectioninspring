package spring.fieldInjection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
class Bottle {
	
	@Autowired
	private Drink drink;
	
	void drinkBottle() {
		drink.drink();
	}
}
