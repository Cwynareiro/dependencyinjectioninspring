package spring.constructorInjection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
class Bottle {
	private Drink drink;
	
	Bottle(Drink drink){
		this.drink=drink;
	}
	
	void drinkBottle() {
		drink.drink();
	}
}
