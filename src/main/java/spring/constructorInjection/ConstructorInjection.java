package spring.constructorInjection;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class ConstructorInjection implements CommandLineRunner{
		
	private Bottle bottle;
	ConstructorInjection(Bottle bottle){
		this.bottle=bottle;
	}
		
	public static void main(String[] args) {
		SpringApplication.run(ConstructorInjection.class, args);
		
		
	}
	@Override
    public void run(String... args) {
		bottle.drinkBottle();
    }

}
