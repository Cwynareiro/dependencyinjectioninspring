package spring.setterInjection;

import org.springframework.stereotype.Component;

@Component
public class CocaCola implements Drink {
	
@Override
public void drink() {
	System.out.println("I'm drinking Cola");
	
}
}
