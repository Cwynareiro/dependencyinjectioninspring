package spring.setterInjection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
class Bottle {
	private Drink drink;
	
	@Autowired
	void setDrink (Drink drink) {
		this.drink=drink;
	}
	public void drinkBottle() {
		drink.drink();
	}
}
