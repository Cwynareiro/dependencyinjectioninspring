package withInjection;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WithInjection {

	public static void main(String[] args) {
		SpringApplication.run(WithInjection.class, args);
		
		Bottle bottle = new Bottle(new CocaCola());
		
		bottle.drinkBottle();
	}

}
