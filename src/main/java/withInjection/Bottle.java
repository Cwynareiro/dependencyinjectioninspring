package withInjection;

public class Bottle {
	
	private Drink drink;
	
	public Bottle(Drink drink) {
		this.drink= drink;
	}
	
	void drinkBottle() {
		drink.drink();
	}
}
